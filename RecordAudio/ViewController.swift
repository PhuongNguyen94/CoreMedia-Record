//
//  ViewController.swift
//  RecordAudio
//
//  Created by Administrator on 1/25/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController,AVAudioPlayerDelegate,AVAudioRecorderDelegate {

    @IBOutlet weak var RecordButton: UIButton!
    @IBOutlet weak var PlayButton: UIButton!
    
    var soundRecorder : AVAudioRecorder?
    var soundPlayer : AVAudioPlayer?
    var fileName = "audioFile.m4a"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupRecorder()
        
    }
    //MARK: - Setting recorder.
    func setupRecorder(){
        let recordSetting = [AVFormatIDKey:kAudioFormatAppleLossless,AVEncoderAudioQualityKey:AVAudioQuality.max.rawValue,AVEncoderBitRateKey:320000,AVNumberOfChannelsKey :2,AVSampleRateKey:44100.0] as [String : Any]
        do {
            try soundRecorder = AVAudioRecorder(url: getfileURL() as URL, settings: recordSetting)
        }catch {
            print("something went wrong")
        }
        soundRecorder?.delegate = self
        soundRecorder?.prepareToRecord()
    }
    func getCacheDirectory() -> NSString{
        let paths = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true) as [NSString]
        return paths[0]
    }
    //MARK : Get url recorder .
    func getfileURL()-> NSURL{
        let path = getCacheDirectory().appendingPathComponent(fileName)
        let filePath = NSURL(fileURLWithPath: path)
        return filePath
    }

    @IBAction func bnt_Record(_ sender: UIButton) {
        if sender.titleLabel?.text == "Record"{
            soundRecorder?.record()
            sender.setTitle("Stop", for: .normal)
            PlayButton.isEnabled = false
        }else{
            soundRecorder?.stop()
            sender.setTitle("Record", for: .normal)
            PlayButton.isEnabled = false
            
        }
    }
    
    @IBAction func bnt_Play(_ sender: UIButton) {
        if sender.titleLabel?.text == "Play"{
            RecordButton.isEnabled = false
            sender.setTitle("Stop", for: .normal)
            
            prepareLayer()
            soundPlayer?.play()
        }else{
            soundPlayer?.stop()
            sender.setTitle("Play", for: .normal)
        }
    }
    func prepareLayer(){
        do {
            try  soundPlayer = AVAudioPlayer(contentsOf: getfileURL() as URL)
        }catch{
            print("something wrong")
        }
        soundPlayer?.delegate = self
        soundPlayer?.volume = 1.0
        soundPlayer?.prepareToPlay()
       
    }
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
        PlayButton.isEnabled = true
    }
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        RecordButton.isEnabled = true
        PlayButton.setTitle("Play", for: .normal)
    }
    
}

